#include "tests/tests.h"

int main() {
  RUN_TEST(malloc)
  RUN_TEST(one_free)
  RUN_TEST(two_free)
  RUN_TEST(region_extends)
  RUN_TEST(region_extends_2)
}