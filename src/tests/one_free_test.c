#include "tests.h"
#include "../mem_internals.h"

#define REGION_SIZE 1000

bool one_free_test() {
  size_t region_size = REGION_SIZE;

  void *heap = heap_init(region_size);
  debug_heap(stdout, heap);

  _malloc((size_t) REGION_SIZE / 10);
  void *element2 = _malloc((size_t) REGION_SIZE / 10);
  _malloc((size_t) REGION_SIZE / 10);

  _free(element2);

  struct block_header* header = (struct block_header*) ((void *) block_get_header(element2));

  if (!header->is_free) {
    return false;
  }

  heap_free(heap, region_size);

  return true;
}