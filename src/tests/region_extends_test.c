#include "tests.h"
#include "../mem_internals.h"

#define REGION_SIZE 8175
#define SMALL_ELEMENT_SIZE 100
#define LARGE_ELEMENT_SIZE 8000

bool region_extends_test() {
  size_t region_size = REGION_SIZE;

  void *heap = heap_init(region_size);
  debug_heap(stdout, heap);

  _malloc((size_t) SMALL_ELEMENT_SIZE);
  void *element2 = _malloc((size_t) SMALL_ELEMENT_SIZE);
  void *element3 = _malloc((size_t) LARGE_ELEMENT_SIZE);

  _free(element3);
  _free(element2);

  struct block_header* header = (struct block_header*) ((void *) block_get_header(element2));

  if (header->capacity.bytes < (size_t) ((double) region_size * 1.5)) {
    return false;
  }

  heap_free(heap, region_size);

  return true;
}