#ifndef ASSIGNMENT_4_MEMORY_ALLOCATOR_TESTS_H
#define ASSIGNMENT_4_MEMORY_ALLOCATOR_TESTS_H

#include "../mem.h"

bool malloc_test();
bool one_free_test();
bool two_free_test();
bool region_extends_test();
bool region_extends_2_test();

#define RUN_TEST(name) \
  printf("-------------------------------\n"); \
  printf("%s test is running...\n", #name); \
  if (name##_test())   \
    printf("\033[1;32mOk\n");      \
  else                 \
    printf("\033[1;31mFail\n"); \
  printf("\033[0m-------------------------------\n");

#endif //ASSIGNMENT_4_MEMORY_ALLOCATOR_TESTS_H
