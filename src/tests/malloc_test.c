#include "tests.h"
#include "../mem_internals.h"

#define REGION_SIZE 1000

bool malloc_test() {
  size_t region_size = REGION_SIZE;

  void *heap = heap_init(region_size);
  debug_heap(stdout, heap);

  void *test_malloc = _malloc((size_t) REGION_SIZE / 10);
  if (!test_malloc) {
    heap_free(heap, region_size);
    return false;
  }
  heap_free(heap, region_size);
  return true;
}