#include "tests.h"
#include "../mem_internals.h"

#define REGION_SIZE 8175
#define ELEMENT_SIZE 2000

bool region_extends_2_test() {
  size_t region_size = REGION_SIZE;

  void *heap = heap_init(region_size);
  debug_heap(stdout, heap);

  void *start = _malloc((size_t) ELEMENT_SIZE);
  _malloc((size_t) ELEMENT_SIZE);
  void *element = _malloc((size_t) ELEMENT_SIZE);
  _malloc((size_t) ELEMENT_SIZE);

  _free(element);

  void *from_new_block = _malloc((size_t) 2 * ELEMENT_SIZE);

  if (from_new_block == NULL) {
    return false;
  }

  struct block_header* header = (struct block_header*) ((void *) block_get_header(start));
  /* No munmap for next block after previous test */
  struct block_header* header_new = (struct block_header*) ((void *) block_get_header(from_new_block));

  if (header + 2 * region_size >= header_new) {
    return false;
  }

  heap_free(heap, region_size);

  return true;
}