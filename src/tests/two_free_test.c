#include "tests.h"
#include "../mem_internals.h"

#define REGION_SIZE 1000

bool two_free_test() {
  size_t region_size = REGION_SIZE;

  void *heap = heap_init(region_size);
  debug_heap(stdout, heap);

  _malloc((size_t) REGION_SIZE / 10);
  void *element2 = _malloc((size_t) REGION_SIZE / 10);
  void *element3 = _malloc((size_t) REGION_SIZE / 10);

  size_t min_capacity = block_get_header(element2)->capacity.bytes + block_get_header(element3)->capacity.bytes
          + block_get_header(element3)->next->capacity.bytes;

  _free(element3);
  _free(element2);

  struct block_header* header = (struct block_header*) ((void *) block_get_header(element2));

  if (!header->is_free || header->next != NULL || header->capacity.bytes < min_capacity) {
    return false;
  }

  heap_free(heap, region_size);

  return true;
}